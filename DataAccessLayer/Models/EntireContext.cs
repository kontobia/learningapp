﻿
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public class EntireContext : IdentityDbContext, IEntireContext
    {


        public EntireContext(): base("name=LearningApp")
        {
           Database.SetInitializer<EntireContext>(new DropCreateDatabaseIfModelChanges<EntireContext>());
        }
        public static EntireContext Create()
        {
            return new EntireContext();
        }


        //add-migration -ProjectName DataAccessLayer -StartUpProjectName LearningApp -ConnectionStringName LearningApp -ConfigurationTypeName ConfigurationEntireContext dupa
        //update-database -ProjectName DataAccessLayer -StartUpProjectName LearningApp -ConnectionStringName LearningApp -ConfigurationTypeName ConfigurationEntireContext
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<LessonElement> LessonElements { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>().ToTable("Users");
            modelBuilder.Entity<IdentityRole>().ToTable("Roles");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaims");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRoles");
        }
 
    }
}