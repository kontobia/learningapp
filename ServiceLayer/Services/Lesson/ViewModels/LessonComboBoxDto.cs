﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class LessonComboBoxDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}