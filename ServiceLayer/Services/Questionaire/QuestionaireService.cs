﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;

namespace ServiceLayer.Services
{
    public class QuestionaireService : IQuestionaireService
    {
        private readonly IEntireContext context;
        public QuestionaireService(IEntireContext context)
        {
            this.context = context;
        }

        public bool CheckIfUserFilledQuestionaire(string userId)
        {
            var result = false;
            try
            {
                var user = GetUserById(userId);
                if (user.LearningType != null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }

            }
            catch (Exception ex)
            {

            }

            return result;
        }

        public bool FillUserLearningType(string userId, LearningTypes type)
        {
            bool result = false;
            try
            {
                var user = GetUserById(userId);
                user.LearningType = type;
                context.SaveChanges();
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public QuestionaireViewModel ReadQuestionaireFromJson()
        {
            QuestionaireViewModel viewModel = null;
            try
            {
                string fileName = "ankieta.json";
                string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
                using (StreamReader r = new StreamReader(path))
                {
                    string json = r.ReadToEnd();
                    viewModel = JsonConvert.DeserializeObject<QuestionaireViewModel>(json);
                }

            }
            catch (Exception e)
            {

            }

            return viewModel;
        }
        public LearningTypes? GetUserLearningType(string userId)
        {
            var learningType = this.context.ApplicationUsers.ToList().FirstOrDefault(q => q.UserId == userId).LearningType;
            return learningType;
        }
        public QuestionaireResultsViewModel InterpretateResults(int[] answers, string userId)
        {
            QuestionaireResultsViewModel result = new QuestionaireResultsViewModel();
            int resultType1Count = 0;
            int resultType2Count = 0;
            int resultType3Count = 0;
            int resultType4Count = 0;

            foreach (var answer in answers)
            {
                if (answer == 1) { resultType1Count++; }
                else if (answer == 2) { resultType1Count++; }
                else if (answer == 3) { resultType1Count++; }
                else if (answer == 4) { resultType1Count++; }
            }

            var max = Math.Max(resultType1Count, resultType2Count);
            max = Math.Max(max, resultType3Count);
            max = Math.Max(max, resultType4Count);

            if (max == resultType1Count)
            {
                result.ComputedLearningStyle = LearningTypes.Odkrywanie_słuchowiec;
            }
            else if (max == resultType2Count)
            {
                result.ComputedLearningStyle = LearningTypes.Odkrywanie_wzrokowiec;
            }
            else if (max == resultType3Count)
            {
                result.ComputedLearningStyle = LearningTypes.Przyswajanie_słuchowiec;
            }
            else if (max == resultType4Count)
            {
                result.ComputedLearningStyle = LearningTypes.Przyswajanie_wzrokowiec;
            }

            var user = GetUserById(userId);
            user.LearningType = result.ComputedLearningStyle;
            context.SaveChanges();
            return result;
        }
        private ApplicationUser GetUserById(string userId)
        {
            var result = this.context.ApplicationUsers.ToList().FirstOrDefault(q => q.UserId == userId);
            return result;
        }
    }
}