﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class LoggerConfiguration : ILoggerConfiguration
    {
        public string GetExtension()
        {
            return ".txt";
        }

        public string GetFileName()
        {
            return "LoggerData";
        }

        public string GetLocation()
        {
            return AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
        }
    }
}