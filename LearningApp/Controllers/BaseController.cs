﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearningApp.Controllers
{
    [Authorize]
    public class BaseController: Controller
    {

        public RedirectToRouteResult RedirectToAction_(string actionName, string controllerName, object routeValues)
        {
            return RedirectToAction(actionName, controllerName, new RouteValueDictionary(routeValues));
        }
       
    }
}