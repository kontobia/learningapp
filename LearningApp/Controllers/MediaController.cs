﻿using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearningApp.Controllers
{
    public class MediaController : Controller
    {
        private readonly IMediaService mediaService;

        public MediaController(IMediaService mediaService)
        {
            this.mediaService = mediaService;
        }
        
        public string RenderImage(long id)
        {
            var file = this.mediaService.GetFileAsByteArray(id);
            var base64 = Convert.ToBase64String(file);
            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            var imageTemplate = string.Format("<img src='{0}' />", imgSrc);

            return imageTemplate;
        }

        public ActionResult GetAudioStream(long id)
        {
            var byteArray = this.mediaService.GetFileAsByteArray(id);
            var memoryStream = new MemoryStream(byteArray);

            return new FileStreamResult(memoryStream, "audio/mp3"); ;
        }

        public ActionResult GetVideoStream(long id)
        {
            var byteArray = this.mediaService.GetFileAsByteArray(id);
            var memoryStream = new MemoryStream(byteArray);

            return new FileStreamResult(memoryStream, "video/mp4"); ;
        }
    }
}