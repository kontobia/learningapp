﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class MediaService : IMediaService
    {
        private readonly IEntireContext context;

        public MediaService(IEntireContext context)
        {
            this.context = context;
        }

        public byte[] GetFileAsByteArray(long id)
        {
            var file = this.context.Files.FirstOrDefault(q => q.Id == id);
            var fileByteArray = file.FileContent;
            return fileByteArray;
        }

        public bool InsertFileAsByteArray(byte[] fileContent, long lessonElementId, MediaType mediaType)
        {
            var result = false;
            try
            {
                var lessonElement = this.context.LessonElements.FirstOrDefault(q => q.Id == lessonElementId);
                lessonElement.File = new DataAccessLayer.Models.File() { FileContent = fileContent, MediaType = mediaType };
                this.context.SaveChanges();
                result = true;
            }
            catch (Exception ex) { }

            return result;
        }

        public byte[] CreateByteArrayFromHttpPostedFileBase(HttpPostedFileBase file)
        {
            MemoryStream target = new MemoryStream();
            file.InputStream.CopyTo(target);
            byte[] data = target.ToArray();

            return data;
        }
    }
}