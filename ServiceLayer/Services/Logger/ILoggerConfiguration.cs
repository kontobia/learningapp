﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public interface ILoggerConfiguration
    {
        string GetFileName();
        string GetExtension();
        string GetLocation();
    }
}
