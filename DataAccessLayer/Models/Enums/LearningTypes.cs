﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models.Enums
{
    public enum LearningTypes : int
    {
        Przyswajanie_wzrokowiec = 1,
        Przyswajanie_słuchowiec = 0,
        Odkrywanie_wzrokowiec = 2,
        Odkrywanie_słuchowiec = 3,
    }
}