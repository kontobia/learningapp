﻿using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class LessonElementComparer : IComparer<LessonElementViewModel>
    {
        public int Compare(LessonElementViewModel x, LessonElementViewModel y)
        {
            var orderBy = x.OrderBy;
            if(x.LessonElementType==orderBy)
            {
                return -1;
            }
            else if (y.LessonElementType == orderBy)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}