﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Enums;
using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public interface ILessonService
    {
        bool CreateLesson(LessonViewModel vm, string userId);
        LessonViewModel GetLesson(long lessonId);
        IEnumerable<LessonViewModel> GetUserLessons(string userId);
        bool UpdateLesson(LessonViewModel vm);
        bool DeleteLesson(long id);
        LessonContentPreviewViewModel GetDataForLessonPreview(long id);
        LessonContentPreviewViewModel GetDataForOrderedLessonPreview(long id, LearningTypes? type);
        LessonContentPreviewViewModel GetSortedDataForLessonPreview(long id, string userId);
        bool CreateLessonElement(LessonElementViewModel vm);
        IEnumerable<LessonComboBoxDto> GetAllLessonsForComboBox();
        IEnumerable<LessonViewModel> GetAllLessons();
        bool RemoveLessonElement(long lessonId, long lessonElementid);
        bool ChangeLessonElementOrder(long lessonId, long lessonElementId, bool increment, LearningTypes type);
        bool AddExistingLessonElementToLesson(int lessonId, int lessonElementId);
        IEnumerable<LessonElementComboDto> GetAllLessonsElementsForComboBox();

        LessonElementViewModel GetLessonElement(int lessonElementId);
    }
}
