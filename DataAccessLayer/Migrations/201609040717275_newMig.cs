namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newMig : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "LearningType", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "LearningType");
        }
    }
}
