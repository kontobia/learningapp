﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Enums;
using LearningApp.Filters;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearningApp.Controllers
{
    [QuestionaryAuthorizeFilter]
    public class LessonController : BaseController
    {
        private ILessonService lessonService;
        private IQuestionaireService questionaireService;

        public LessonController(ILessonService lessonService, IQuestionaireService questionaireService)
        {
            this.lessonService = lessonService;
            this.questionaireService = questionaireService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var vm = lessonService.GetUserLessons(userId);
            return View(vm);
        }

        [HttpGet]
        public ActionResult LessonsPanel()
        {
            var vm = lessonService.GetAllLessons();
            return View(vm);
        }

        [HttpGet]
        public ActionResult LessonLearn(long id)
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var learnType = questionaireService.GetUserLearningType(userId);
            var vm = lessonService.GetDataForOrderedLessonPreview(id, learnType);
            return View(vm);
        }

        [HttpGet]
        public ActionResult CreateLesson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateLesson(LessonViewModel vm)
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                lessonService.CreateLesson(vm, userId);
            }
            else
            {
                return View(vm);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(long id)
        {
            var vm = lessonService.GetLesson(id);
            return View("EditLesson", vm);
        }

        [HttpPost]
        public ActionResult Edit(LessonViewModel vm)
        {
            if (ModelState.IsValid)
            {
                lessonService.UpdateLesson(vm);
            }
            else
            {
                return View("EditLesson", vm);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ConfirmDelete(long id)
        {
            return View(id);
        }

        [HttpPost]
        public ActionResult Delete(long id)
        {
            lessonService.DeleteLesson(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult PreviewLessonContent(long id, bool? ordered, LearningTypes? type)
        {
            LessonContentPreviewViewModel vm = new LessonContentPreviewViewModel();
            if(ordered==true)
            {
                vm = lessonService.GetDataForOrderedLessonPreview(id, type);
            }
            else
            {
                vm = lessonService.GetDataForLessonPreview(id);
            }
            
            return PartialView("PreviewLesson", vm);
        }

        [HttpGet]
        public ActionResult CreateLessonElement(long? id)
        {
            if(id!=null)
            {
                return PartialView(new LessonElementViewModel() { LessonId=id.Value,});
            }
            return PartialView();
        }

        [HttpPost]
        public ActionResult CreateLessonElement(LessonElementViewModel vm)
        {
            Object response = null;
            if (ModelState.IsValid)
            {
                var result = lessonService.CreateLessonElement(vm);
                response = result == true ?
                    new { ResponseType = JsonResponseType.Success, AdditionalData = "" } :
                    new { ResponseType = JsonResponseType.Error, AdditionalData = "Error occured during lesson element creation." };
            }
            else
            {
                return View(vm);
            }

            return Json(response);
        }

        [HttpGet]
        public ActionResult AddExistingLessonElement(long lessonId)
        {
            return View(lessonId);
        }

        [HttpPost]
        public ActionResult AddExistingLessonElement(int lessonId, int lessonElementId)
        {
            bool result = this.lessonService.AddExistingLessonElementToLesson(lessonId, lessonElementId);
            object response = result == true ?
                    new { ResponseType = JsonResponseType.Success, AdditionalData = "" } :
                    new { ResponseType = JsonResponseType.Error, AdditionalData = "Error occured during lesson element deletion." };
            return Json(response);
        }

        [HttpPost]
        public ActionResult RemoveLessonElement(long lessonId, long lessonElementId)
        {
            bool result = lessonService.RemoveLessonElement(lessonId, lessonElementId);
            object response = result == true ?
                    new { ResponseType = JsonResponseType.Success, AdditionalData = "" } :
                    new { ResponseType = JsonResponseType.Error, AdditionalData = "Error occured during lesson element deletion." };
            return Json(response);
        }

        [HttpGet]
        public ActionResult ModifyLessonContent()
        {
            return View();
        }


        public ActionResult GetLessonsForAutoComplete()
        {
            var data = lessonService.GetAllLessonsForComboBox();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLessonsElementsForAutoComplete()
        {
            var data = lessonService.GetAllLessonsElementsForComboBox();
            return Json(data.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult LessonOrderItems()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult ChangeLessonElementOrder(long lessonId, long lessonElementId, bool increment, LearningTypes type)
        {
            bool result = lessonService.ChangeLessonElementOrder(lessonId, lessonElementId, increment, type);
            object response = result == true ?
                    new { ResponseType = JsonResponseType.Success, AdditionalData = "Lesson element order changed" } :
                    new { ResponseType = JsonResponseType.Error, AdditionalData = "Error occured during lesson element deletion." };
            return Json(response);
        }

        [HttpPost]
        public ActionResult PreviewSingleLessonElement(int lessonElementId)
        {
            LessonElementViewModel vm = this.lessonService.GetLessonElement(lessonElementId);
            return View(vm);
        }
    }
}