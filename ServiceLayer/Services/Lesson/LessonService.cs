﻿using DataAccessLayer.Models;
using ServiceLayer.Services;
using ServiceLayer.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Core;
using DataAccessLayer.Models.Enums;

namespace ServiceLayer.Services
{


    public class LessonService : ILessonService
    {
        private readonly IEntireContext context;
        private readonly ILogger logger;
        private readonly IQuestionaireService questionaireService;
        private readonly IMediaService mediaService;
        public LessonService(IEntireContext context, ILogger logger, IQuestionaireService questionaireService, IMediaService mediaService)
        {
            this.context = context;
            this.logger = logger;
            this.questionaireService = questionaireService;
            this.mediaService = mediaService;
        }

        public bool CreateLesson(LessonViewModel vm, string userId)
        {
            bool result;
            try
            {
                this.context.Lessons.Add(new Lesson()
                {
                    Name = vm.Name,
                    Description = vm.Description,
                    CreatorId = userId,
                });

                this.context.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {
                result = false;
                //logger.writetolog(e.message);
            }

            return result;
        }
        public IEnumerable<LessonViewModel> GetUserLessons(string userId)
        {
            var data = this.context.Lessons.Where(q => q.CreatorId == userId).Select(q => new LessonViewModel()
            {
                Description = q.Description,
                Id = q.Id,
                Name = q.Name,
                CreatorFullName = q.Creator.FirstName + " " + q.Creator.LastName
            }).ToList();

            return data;
        }
        public IEnumerable<LessonViewModel> GetAllLessons()
        {
            var data = this.context.Lessons.Select(q => new LessonViewModel()
            {
                Description = q.Description,
                Id = q.Id,
                Name = q.Name,
                CreatorFullName = q.Creator.FirstName + " " + q.Creator.LastName
            }).ToList();

            return data;
        }
        public LessonViewModel GetLesson(long lessonId)
        {
            var lesson = this.context.Lessons.Single(q => q.Id == lessonId);
            var data = new LessonViewModel()
            {
                Id = lesson.Id,
                Description = lesson.Description,
                Name = lesson.Name,
                CreatorFullName = lesson.Creator.FirstName + " " + lesson.Creator.LastName
            };

            return data;
        }
        public bool UpdateLesson(LessonViewModel vm)
        {
            bool result;

            try
            {
                var oldEntity = this.context.Lessons.SingleOrDefault(q => q.Id == vm.Id);
                oldEntity.Description = vm.Description;
                oldEntity.Name = vm.Name;
                result = true;
                this.context.SaveChanges();
            }
            catch (Exception e)
            {
                result = false;
                logger.WriteToLog(e.Message);
            }

            return result;
        }
        public bool DeleteLesson(long id)
        {
            bool result;
            try
            {
                var entityToDelete = this.context.Lessons.Find(id);
                foreach (var lessonElement in entityToDelete.LessonElements)
                {
                    lessonElement.LessonOrders.Clear();
                }
                this.context.Lessons.Remove(entityToDelete);
                result = true;
                this.context.SaveChanges();
            }
            catch (Exception e)
            {
                result = false;
                //logger.WriteToLog(e.Message);
            }

            return result;
        }
        public LessonContentPreviewViewModel GetDataForLessonPreview(long id)
        {
            try
            {
                var lessonContentPreview = (from lesson in this.context.Lessons.Where(q => q.Id == id)
                                            select new LessonContentPreviewViewModel()
                                            {
                                                Id = lesson.Id,
                                                Name = lesson.Name,
                                                Description = lesson.Description,
                                                CreatorFullName = lesson.Creator.FirstName + " " + lesson.Creator.LastName,
                                            }).Single();

                List<LessonElementViewModel> lessonElementsList = new List<LessonElementViewModel>();

                var lessonElements = this.context.Lessons.Find(id).LessonElements.ToList();
                foreach (var lessonElement in lessonElements)
                {
                    var newElement = new LessonElementViewModel()
                    {
                        Id = lessonElement.Id,
                        LessonId = lessonContentPreview.Id,
                        TextContent = lessonElement.TextContent,
                        LessonElementType = lessonElement.LessonElementType,
                        Identifier = lessonElement.Identifier,
                        FileId = lessonElement.FileId,
                    };

                    lessonElementsList.Add(newElement);
                }
                lessonContentPreview.LessonElements = lessonElementsList;
                return lessonContentPreview;


            }
            catch (Exception e)
            {
                //logger.WriteToLog(e.Message);
            }

            return null;
        }

        public LessonContentPreviewViewModel GetDataForOrderedLessonPreview(long id, LearningTypes? type)
        {
            try
            {
                var lessonContentPreview = (from lesson in this.context.Lessons.Where(q => q.Id == id)
                                            select new LessonContentPreviewViewModel()
                                            {
                                                Id = lesson.Id,
                                                Name = lesson.Name,
                                                Description = lesson.Description,
                                                CreatorFullName = lesson.Creator.FirstName + " " + lesson.Creator.LastName,
                                            }).Single();

                List<LessonElementViewModel> lessonElementsList = new List<LessonElementViewModel>();

                var lessonElements = this.context.Lessons.Find(id).LessonElements.ToList();
                foreach (var lessonElement in lessonElements)
                {
                    var lessonOrder = lessonElement.LessonOrders.FirstOrDefault(q => q.LearningType == type);
                    var newElement = new LessonElementViewModel()
                    {
                        Id = lessonElement.Id,
                        LessonId = lessonContentPreview.Id,
                        TextContent = lessonElement.TextContent,
                        LessonElementType = lessonElement.LessonElementType,
                        Identifier = lessonElement.Identifier,
                        FileId = lessonElement.FileId,
                        CurrentOrder = lessonOrder == null ? 0 : lessonOrder.Order
                    };

                    lessonElementsList.Add(newElement);
                }
                lessonContentPreview.LessonElements = lessonElementsList.OrderByDescending(q => q.CurrentOrder);
                return lessonContentPreview;


            }
            catch (Exception e)
            {
                //logger.WriteToLog(e.Message);
            }

            return null;
        }

        public LessonContentPreviewViewModel GetSortedDataForLessonPreview(long id, string userId)
        {
            try
            {
                var lessonContentPreview = (from lesson in this.context.Lessons.Where(q => q.Id == id)
                                            select new LessonContentPreviewViewModel()
                                            {
                                                Id = lesson.Id,
                                                Name = lesson.Name,
                                                Description = lesson.Description,
                                                CreatorFullName = lesson.Creator.FirstName + " " + lesson.Creator.LastName,
                                            }).Single();

                List<LessonElementViewModel> lessonElementsList = new List<LessonElementViewModel>();

                var lessonElements = this.context.Lessons.Find(id).LessonElements.ToList();
                foreach (var lessonElement in lessonElements)
                {
                    var newElement = new LessonElementViewModel()
                    {
                        Id = lessonElement.Id,
                        LessonId = lessonContentPreview.Id,
                        TextContent = lessonElement.TextContent,
                        LessonElementType = lessonElement.LessonElementType,
                        Identifier = lessonElement.Identifier,
                        FileId = lessonElement.FileId,
                        // OrderBy = questionaireService.GetUserLearningType(userId).Value
                    };

                    lessonElementsList.Add(newElement);
                }
                LessonElementComparer comparer = new LessonElementComparer();
                lessonElementsList.Sort(comparer);
                lessonContentPreview.LessonElements = lessonElementsList;
                return lessonContentPreview;


            }
            catch (Exception e)
            {
                //logger.WriteToLog(e.Message);
            }

            return null;
        }

        public bool CreateLessonElement(LessonElementViewModel vm)
        {
            bool result = false;
            try
            {
                List<LessonOrder> lessonOrdersList = GenerateBasicLessonOrderList();
                var lesson = this.context.Lessons.FirstOrDefault(q => q.Id == vm.LessonId);
                var entity = new LessonElement()
                {
                    TextContent = vm.TextContent,
                    LessonElementType = vm.LessonElementType,
                    LessonOrders = lessonOrdersList,
                    Identifier =vm.Identifier,
                };
                lesson.LessonElements.Add(entity);
                this.context.SaveChanges();
                MediaType mediaType;
                switch (vm.LessonElementType)
                {
                    case LessonElementType.Image:
                        mediaType = MediaType.Image;
                        break;
                    case LessonElementType.Audio:
                        mediaType = MediaType.Audio;
                        break;
                    case LessonElementType.Video:
                        mediaType = MediaType.Video;
                        break;
                    default:
                        mediaType = MediaType.Audio;
                        break;
                }
                var fileContent = this.mediaService.CreateByteArrayFromHttpPostedFileBase(vm.File);
                this.mediaService.InsertFileAsByteArray(fileContent, entity.Id, mediaType);
                this.context.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {
                //this.logger.WriteToLog(e.Message);
                result = false;
            }

            return result;
        }

        private static List<LessonOrder> GenerateBasicLessonOrderList()
        {
            var lessonOrdersList = new List<LessonOrder>();
            lessonOrdersList.Add(new LessonOrder() { LearningType = LearningTypes.Odkrywanie_słuchowiec, Order = 1 });
            lessonOrdersList.Add(new LessonOrder() { LearningType = LearningTypes.Odkrywanie_wzrokowiec, Order = 1 });
            lessonOrdersList.Add(new LessonOrder() { LearningType = LearningTypes.Przyswajanie_słuchowiec, Order = 1 });
            lessonOrdersList.Add(new LessonOrder() { LearningType = LearningTypes.Przyswajanie_wzrokowiec, Order = 1 });
            return lessonOrdersList;
        }

        public bool RemoveLessonElement(long lessonId, long lessonElementid)
        {
            var result = false;
            try
            {
                var entityToRemove = this.context.LessonElements.FirstOrDefault(q => q.Id == lessonElementid);
                this.context.Lessons.FirstOrDefault(q => q.Id == lessonId).LessonElements.Remove(entityToRemove);
                this.context.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {

            }

            return result;
        }

        public IEnumerable<LessonElementComboDto> GetAllLessonsElementsForComboBox()
        {
            var data = this.context.LessonElements.Where(q=>q.Identifier!=null).Select(q => new LessonElementComboDto()
            {
                Id = q.Id,
                Name = q.Identifier
            }).ToList();

            return data;
        }

        public IEnumerable<LessonComboBoxDto> GetAllLessonsForComboBox()
        {
            var data = this.context.Lessons.Select(q => new LessonComboBoxDto()
            {
                Id = q.Id,
                Name = q.Name
            }).ToList();

            return data;
        }

        public bool ChangeLessonElementOrder(long lessonId, long lessonElementId, bool increment, LearningTypes type)
        {
            var result = false;
            //var change = increment == true ? 1 : -1;

            try
            {
                var lessonElement = this.context.LessonElements.FirstOrDefault(q => q.Id == lessonElementId);
                var lessonElementCount = this.context.Lessons.FirstOrDefault(q => q.Id == lessonId).LessonElements.Count();
                var lessonOrderItem = lessonElement.LessonOrders.FirstOrDefault(q => q.LearningType == type);
                if (lessonOrderItem != null)
                {
                    if (increment)
                    {
                        //change old element order
                        var newOrder = lessonOrderItem.Order + 1;
                        if (lessonElementCount >= newOrder)
                        {
                            var upperLessonElement = this.context.Lessons.FirstOrDefault(q => q.Id == lessonId)
                                                     .LessonElements.FirstOrDefault(q => q.LessonOrders.FirstOrDefault(t => t.LearningType == type && t.Order >= lessonOrderItem.Order && t.Id != lessonOrderItem.Id) != null);
                            var upperElementOrder = upperLessonElement.LessonOrders.FirstOrDefault(q => q.LearningType == type);
                            if (upperElementOrder != null)
                            {
                                upperElementOrder.Order--;
                            }
                            lessonOrderItem.Order++;
                        }
                    }
                    else
                    {
                        //change old element order
                        var newOrder = lessonOrderItem.Order - 1;
                        if (0 <= newOrder)
                        {
                            var lowerElement = this.context.Lessons.FirstOrDefault(q => q.Id == lessonId)
                                                     .LessonElements.FirstOrDefault(q => q.LessonOrders.FirstOrDefault(t => t.LearningType == type && t.Order <= lessonOrderItem.Order && t.Id != lessonOrderItem.Id) != null);
                            var lowerElementOrderItem = lowerElement.LessonOrders.FirstOrDefault(q => q.LearningType == type);
                            if (lowerElementOrderItem != null)
                            {
                                lowerElementOrderItem.Order++;
                            }
                            lessonOrderItem.Order--;
                        }
                    }

                }
                else
                {
                    lessonElement.LessonOrders.Add(new LessonOrder() { LearningType = type, Order = 0 });
                }

                this.context.SaveChanges();
                result = true;
            }
            catch
            {
                result = false;
            }

            return result;
        }

        public bool AddExistingLessonElementToLesson(int lessonId, int lessonElementId)
        {
            var result = false;
            try
            {
                var lessonElement = this.context.LessonElements.FirstOrDefault(q => q.Id == lessonElementId);
                LessonElement nLE = new LessonElement()
                {
                    File = lessonElement.File,
                    FileId = lessonElement.FileId,
                    Identifier = lessonElement.Identifier,
                    LessonElementType = lessonElement.LessonElementType,
                    LessonOrders = GenerateBasicLessonOrderList(),
                    TextContent = lessonElement.TextContent,
                };
                var lesson = this.context.Lessons.FirstOrDefault(q => q.Id == lessonId);
                lesson.LessonElements.Add(nLE);
                this.context.SaveChanges();
                result = true;
            }
            catch (Exception e)
            {

            }

            return result;
        }
        public LessonElementViewModel GetLessonElement(int lessonElementId)
        {
            var entity = this.context.LessonElements.FirstOrDefault(q => q.Id == lessonElementId);
            var data = new LessonElementViewModel() {
                Id=entity.Id,
                FileId=entity.FileId,
                Identifier=entity.Identifier,
                LessonElementType=entity.LessonElementType,
                TextContent=entity.TextContent,
                
            };

            return data;
        }
    }
}