﻿using LearningApp.Controllers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;
using System.Web.Routing;
using System.Web.Security;

namespace LearningApp.Filters
{
    public class QuestionaryAuthorizeFilter : System.Web.Mvc.ActionFilterAttribute
    {
        private IQuestionaireService questionaireService;
        public QuestionaryAuthorizeFilter()
        {
            this.questionaireService = DependencyResolver.Current.GetService<IQuestionaireService>();
        }

        public override void OnActionExecuting(System.Web.Mvc.ActionExecutingContext filterContext)
        {
            this.questionaireService = DependencyResolver.Current.GetService<IQuestionaireService>();
            var userId = HttpContext.Current.User.Identity.GetUserId();
            var userFilledQuestionaire = questionaireService.CheckIfUserFilledQuestionaire(userId);
            if(!userFilledQuestionaire)
            {
                var values = new System.Web.Routing.RouteValueDictionary(new
                {
                    Action = "FillQuestionaire",
                    Controller = "Questionaire"
                });
                //var requestContext = new System.Web.Routing.RequestContext(filterContext.HttpContext, filterContext.RouteData);
                //var vpd = RouteTable.Routes.GetVirtualPath(requestContext, values);
                //var target = vpd.VirtualPath;
                //var url = HttpUtility.UrlEncode(filterContext.HttpContext.Request.Url.PathAndQuery);

                filterContext.Result = new RedirectToRouteResult("Default", values);
                base.OnActionExecuting(filterContext);
            }
        }
    }
}