
CREATE TABLE dbo.Files(
    [id] [bigint] IDENTITY(1,1) NOT NULL,
    [IdFile] [uniqueidentifier] unique ROWGUIDCOL  NOT NULL,
    [Title] [nvarchar](max) NULL,
    [FileContent] [varbinary](max) FILESTREAM  NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([id] ASC))
 
GO
 
ALTER TABLE [dbo].[Files] ADD  CONSTRAINT [DF_Files_IdFile]  DEFAULT (newid()) FOR [IdFile]
GO

