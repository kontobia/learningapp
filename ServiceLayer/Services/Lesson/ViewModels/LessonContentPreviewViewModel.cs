﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class LessonContentPreviewViewModel
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        public string CreatorFullName { get; set; }

        public IEnumerable<LessonElementViewModel> LessonElements { get; set; }
    }
}