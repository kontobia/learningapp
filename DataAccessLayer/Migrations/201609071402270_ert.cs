namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ert : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LessonOrders",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LearningType = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                        LessonElement_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LessonElements", t => t.LessonElement_Id)
                .Index(t => t.LessonElement_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LessonOrders", "LessonElement_Id", "dbo.LessonElements");
            DropIndex("dbo.LessonOrders", new[] { "LessonElement_Id" });
            DropTable("dbo.LessonOrders");
        }
    }
}
