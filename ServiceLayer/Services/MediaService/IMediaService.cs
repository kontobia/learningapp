﻿using DataAccessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ServiceLayer.Services
{
    public interface IMediaService
    {
        byte[] GetFileAsByteArray(long id);
        bool InsertFileAsByteArray(byte[] fileContent, long lessonElementId, MediaType mediaType);
        byte[] CreateByteArrayFromHttpPostedFileBase(HttpPostedFileBase file);
    }
}
