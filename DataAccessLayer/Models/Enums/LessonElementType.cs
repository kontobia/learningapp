﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public enum LessonElementType
    {
        Text=0,
        Image=1,
        Video=2,
        Audio=3,
    }
}