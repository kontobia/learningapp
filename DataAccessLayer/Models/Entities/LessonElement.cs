﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public class LessonElement : IEntity
    {
        public long Id { get; set; }
        public long? FileId { get; set; }
        public string Identifier { get; set; }
        public string TextContent { get; set; }
        public LessonElementType LessonElementType { get; set; }
 
        [ForeignKey("FileId")]
        public virtual File File {get;set;}
        public virtual ICollection<LessonOrder> LessonOrders { get; set; }
        public virtual ICollection<Lesson> LessonsContainingThisElement { get; set; }
    }
}