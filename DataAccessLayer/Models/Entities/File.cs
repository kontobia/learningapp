﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    [Table("Files")]
    public class File :IEntity
    {
        public long Id { get; set; }
        public MediaType MediaType { get; set; }
        public byte[] FileContent { get; set; }

        public virtual ICollection<LessonElement> LessonsContainingFile { get; set; }
    }
}