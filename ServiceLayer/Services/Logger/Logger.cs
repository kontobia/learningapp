﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services.Logger
{
    public class Logger : ILogger
    {
        private string FileName;
        private string Extension;
        private string FileLocation;

        private readonly ILoggerConfiguration config;

        public Logger(ILoggerConfiguration config)
        {
            this.config = config;
            this.FileLocation = config.GetLocation();
            this.Extension = config.GetExtension();
            this.FileName = config.GetFileName();
        }

        public void WriteToLog(string text)
        {
            string message = "[" + DateTime.Now + "] " + text + Environment.NewLine;
            File.WriteAllText(FileLocation, message);
        }
    }
}