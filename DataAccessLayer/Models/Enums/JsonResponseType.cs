﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public enum JsonResponseType
    {
        Success=1,
        Error=0,
        Warning=2,
        Info=3,
    }
}