﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Models
{
    public interface IEntireContext
    {
        DbSet<Lesson> Lessons { get; set; }
        DbSet<LessonElement> LessonElements { get; set; }
        DbSet<File> Files { get; set; }
        DbSet<ApplicationUser> ApplicationUsers { get; set; }
        int SaveChanges();
    }
}
