﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public enum MediaType
    {
        Image =0,
        Audio =1,
        Video =2,
    }
}