namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newEra2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.LessonElements", "LessonId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LessonElements", "LessonId", c => c.Long(nullable: false));
        }
    }
}
