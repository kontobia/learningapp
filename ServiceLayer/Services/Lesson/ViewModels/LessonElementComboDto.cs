﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class LessonElementComboDto
    {

        public long Id { get; set; }
        public string Name { get; set; }

    }
}