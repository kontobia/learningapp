﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceLayer.Services;
using DataAccessLayer.Models;
using LearningApp.Controllers;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Logging;
using ServiceLayer.Services.Logger;

namespace LearningApp.Autofac
{
    public class AutofacConfig
    {
        public static void RegisterAutofac()
        {
            var builder = new ContainerBuilder();
 
            builder.RegisterType<EntireContext>().As<IEntireContext>();
            builder.RegisterType<LessonService>().As<ILessonService>();
            builder.RegisterType<LessonElementService>().As<ILessonElementService>();
            builder.RegisterType<Logger>().As<ServiceLayer.Services.Logger.ILogger>();
            builder.RegisterType<LoggerConfiguration>().As<ILoggerConfiguration>();
            builder.RegisterType<QuestionaireService>().As<IQuestionaireService>();
            builder.RegisterType<MediaService>().As<IMediaService>();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}