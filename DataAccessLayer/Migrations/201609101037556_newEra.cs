namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newEra : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Audios", "FileId", "dbo.Files");
            DropForeignKey("dbo.LessonElements", "AudioId", "dbo.Audios");
            DropForeignKey("dbo.Images", "FileId", "dbo.Files");
            DropForeignKey("dbo.LessonElements", "ImageId", "dbo.Images");
            DropForeignKey("dbo.LessonElements", "LessonId", "dbo.Lessons");
            DropForeignKey("dbo.QuestionAnswers", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.LessonElements", "QuestionId", "dbo.Questions");
            DropForeignKey("dbo.LessonElements", "TextId", "dbo.Texts");
            DropIndex("dbo.LessonElements", new[] { "QuestionId" });
            DropIndex("dbo.LessonElements", new[] { "TextId" });
            DropIndex("dbo.LessonElements", new[] { "ImageId" });
            DropIndex("dbo.LessonElements", new[] { "AudioId" });
            DropIndex("dbo.LessonElements", new[] { "LessonId" });
            DropIndex("dbo.Audios", new[] { "FileId" });
            DropIndex("dbo.Images", new[] { "FileId" });
            DropIndex("dbo.QuestionAnswers", new[] { "QuestionId" });
            CreateTable(
                "dbo.LessonLessonElements",
                c => new
                    {
                        Lesson_Id = c.Long(nullable: false),
                        LessonElement_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.Lesson_Id, t.LessonElement_Id })
                .ForeignKey("dbo.Lessons", t => t.Lesson_Id, cascadeDelete: true)
                .ForeignKey("dbo.LessonElements", t => t.LessonElement_Id, cascadeDelete: true)
                .Index(t => t.Lesson_Id)
                .Index(t => t.LessonElement_Id);
            
            AddColumn("dbo.Files", "MediaType", c => c.Int(nullable: false));
            AddColumn("dbo.LessonElements", "Identifier", c => c.String());
            AddColumn("dbo.LessonElements", "TextContent", c => c.String());
            CreateIndex("dbo.LessonElements", "FileId");
            AddForeignKey("dbo.LessonElements", "FileId", "dbo.Files", "Id");
            DropColumn("dbo.LessonElements", "QuestionId");
            DropColumn("dbo.LessonElements", "TextId");
            DropColumn("dbo.LessonElements", "ImageId");
            DropColumn("dbo.LessonElements", "AudioId");
            DropTable("dbo.Audios");
            DropTable("dbo.Images");
            DropTable("dbo.Questions");
            DropTable("dbo.QuestionAnswers");
            DropTable("dbo.Texts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Texts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TextContent = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.QuestionAnswers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuestionAnswerText = c.String(),
                        IsValidAnswer = c.Boolean(nullable: false),
                        QuestionId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        QuestionText = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Images",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(),
                        Title = c.String(),
                        FileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Audios",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Description = c.String(),
                        Title = c.String(),
                        FileId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.LessonElements", "AudioId", c => c.Long());
            AddColumn("dbo.LessonElements", "ImageId", c => c.Long());
            AddColumn("dbo.LessonElements", "TextId", c => c.Long());
            AddColumn("dbo.LessonElements", "QuestionId", c => c.Long());
            DropForeignKey("dbo.LessonLessonElements", "LessonElement_Id", "dbo.LessonElements");
            DropForeignKey("dbo.LessonLessonElements", "Lesson_Id", "dbo.Lessons");
            DropForeignKey("dbo.LessonElements", "FileId", "dbo.Files");
            DropIndex("dbo.LessonLessonElements", new[] { "LessonElement_Id" });
            DropIndex("dbo.LessonLessonElements", new[] { "Lesson_Id" });
            DropIndex("dbo.LessonElements", new[] { "FileId" });
            DropColumn("dbo.LessonElements", "TextContent");
            DropColumn("dbo.LessonElements", "Identifier");
            DropColumn("dbo.Files", "MediaType");
            DropTable("dbo.LessonLessonElements");
            CreateIndex("dbo.QuestionAnswers", "QuestionId");
            CreateIndex("dbo.Images", "FileId");
            CreateIndex("dbo.Audios", "FileId");
            CreateIndex("dbo.LessonElements", "LessonId");
            CreateIndex("dbo.LessonElements", "AudioId");
            CreateIndex("dbo.LessonElements", "ImageId");
            CreateIndex("dbo.LessonElements", "TextId");
            CreateIndex("dbo.LessonElements", "QuestionId");
            AddForeignKey("dbo.LessonElements", "TextId", "dbo.Texts", "Id");
            AddForeignKey("dbo.LessonElements", "QuestionId", "dbo.Questions", "Id");
            AddForeignKey("dbo.QuestionAnswers", "QuestionId", "dbo.Questions", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LessonElements", "LessonId", "dbo.Lessons", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LessonElements", "ImageId", "dbo.Images", "Id");
            AddForeignKey("dbo.Images", "FileId", "dbo.Files", "Id", cascadeDelete: true);
            AddForeignKey("dbo.LessonElements", "AudioId", "dbo.Audios", "Id");
            AddForeignKey("dbo.Audios", "FileId", "dbo.Files", "Id", cascadeDelete: true);
        }
    }
}
