﻿using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Models
{
    public class LessonOrder
    {
        public long Id { get; set; }
        public LearningTypes LearningType { get; set; }
        public int Order { get; set; }
    }
}