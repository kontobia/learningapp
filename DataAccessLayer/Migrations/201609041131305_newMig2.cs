namespace DataAccessLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newMig2 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.LessonElements", "Name");
            DropColumn("dbo.LessonElements", "Description");
            DropColumn("dbo.LessonElements", "Title");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LessonElements", "Title", c => c.String());
            AddColumn("dbo.LessonElements", "Description", c => c.String());
            AddColumn("dbo.LessonElements", "Name", c => c.String());
        }
    }
}
