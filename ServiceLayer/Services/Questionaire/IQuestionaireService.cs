﻿using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServiceLayer.Services
{
    public interface IQuestionaireService
    {
        bool CheckIfUserFilledQuestionaire(string userId);
        bool FillUserLearningType(string userId, LearningTypes type);
        QuestionaireViewModel ReadQuestionaireFromJson();
        QuestionaireResultsViewModel InterpretateResults(int[] answers, string userId);
        LearningTypes? GetUserLearningType(string userId);

    }
}
