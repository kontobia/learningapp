﻿var notificationScope = (function () {

    function CreateNotification(message, icon, title, messageType) {
        $.notify({ message: message, icon: icon, title: title, message: message }, {
            type: messageType, placement: {
                from: "top",
                align: "center"
            },
        });
    }
    return {
        CreateNotification: CreateNotification,
    }
}());