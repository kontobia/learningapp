﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace DataAccessLayer.Migrations
{
    internal sealed class ConfigurationEntireContext : DbMigrationsConfiguration<Models.EntireContext>
    {
        public ConfigurationEntireContext()
        {
            AutomaticMigrationsEnabled = true;
        }
    }
}