﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class QuestionViewModel
    {
        public string Question { get; set; }
        public List<QuestionAnswerViewModel> Answers { get; set; }
    }
}