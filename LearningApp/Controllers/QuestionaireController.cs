﻿using Microsoft.AspNet.Identity;
using ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LearningApp.Controllers
{
    public class QuestionaireController : Controller
    {
        private readonly IQuestionaireService questionaireService;
        public QuestionaireController(IQuestionaireService questionaireService)
        {
            this.questionaireService = questionaireService;
        }

        [HttpGet]
        public ActionResult FillQuestionaire()
        {
            var questionaireViewModel = questionaireService.ReadQuestionaireFromJson();
            return View(questionaireViewModel);
        }

        [HttpPost]
        public ActionResult FillQuestionaire(int[] answers)
        {
            var userId = System.Web.HttpContext.Current.User.Identity.GetUserId();
            var questionaireResult = questionaireService.InterpretateResults(answers, userId);
            return View("QuestionaireResults", questionaireResult.ComputedLearningStyle);
        }

    }
}