﻿using DataAccessLayer.Models;
using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ServiceLayer.Services
{
    public class LessonElementViewModel
    {
        public long Id { get; set; }
        [Required]
        public long LessonId { get; set; }
        public long? FileId { get; set; }
        public string Identifier { get; set; }
        [AllowHtml]
        public string TextContent { get; set; }
        public HttpPostedFileBase File { get; set; }
        public LessonElementType LessonElementType { get; set; }
        public LessonElementType OrderBy { get; set; }

        public long CurrentOrder { get; set; }
        public virtual ICollection<LessonOrder> LessonOrders { get; set; }
    }
}