﻿using DataAccessLayer.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiceLayer.Services
{
    public class QuestionAnswerViewModel
    {
        public string Answer { get; set; }
        public LearningTypes AnswerType { get; set; }
    }
}